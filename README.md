# Flipstarter P2SH

Currently this is research into p2sh / smart contract techniques that will improve Flipstarter user experience while remaining non-custodial and as trustless as possible.


## Contributing

You are welcome to propose new techniques or improvements of the existing ones.
We would be happy to talk and you can reach us at one of the contact options below.


## License

The content of this repository is released under the terms of the MIT license.
See [LICENSE](./LICENSE) for more details.


## Disclosure Policy

Please initiate a private conversation with the maintainer through one of the contact options below.


## Contact

- [Telegram](https://t.me/flipstarter)
- [Email](mailto:contact@flipstarter.cash)
